ARG BASE_IMAGE=docker.io/library/debian:12.9-slim

FROM docker.io/library/golang:1.24-bookworm AS builder

WORKDIR /app
# Copy the Go source code
COPY src/auto-define-backend /app/
# Build the Go binary with static linking
RUN go build -o gitlab-tofu-auto-define-backend .

FROM $BASE_IMAGE

ARG TARGETARCH

RUN apt-get update && apt-get install -y \
  curl \
  wget \
  git \
  jq \
  openssh-client \
  unzip \
  idn2 \
  && rm -rf /var/lib/apt/lists/*

# NOTE: cosign is not yet available in the debian apt sources
ARG COSIGN_VERSION=2.4.3
WORKDIR /tmp
RUN wget https://github.com/sigstore/cosign/releases/download/v${COSIGN_VERSION}/cosign_${COSIGN_VERSION}_${TARGETARCH}.deb && \
    dpkg -i *.deb && \
    rm -f /tmp/*.deb

# NOTE: glab is not yet available in the debian apt sources
ARG GLAB_VERSION=1.53.0
WORKDIR /tmp
RUN wget https://gitlab.com/gitlab-org/cli/-/releases/v${GLAB_VERSION}/downloads/glab_${GLAB_VERSION}_linux_${TARGETARCH}.deb && \
    dpkg -i *.deb && \
    rm -f /tmp/*.deb

# Install OpenTofu using the installer script in standalone mode
# see https://opentofu.org/docs/intro/install/standalone
# We may want to switch to installing manually from GitHub and verifying signature
ARG OPENTOFU_VERSION
RUN curl --proto '=https' --tlsv1.2 -fsSL https://get.opentofu.org/install-opentofu.sh -o install-opentofu.sh && \
    chmod +x install-opentofu.sh && \
    ./install-opentofu.sh --install-method standalone --opentofu-version "${OPENTOFU_VERSION}" && \
    rm ./install-opentofu.sh && \
    rm -rf /tmp/* && \
    tofu --version

WORKDIR /

COPY --chmod=755 src/gitlab-tofu.sh /usr/bin/gitlab-tofu
COPY --from=builder --chmod=755 /app/gitlab-tofu-auto-define-backend /usr/bin/

# Override ENTRYPOINT
ENTRYPOINT []
